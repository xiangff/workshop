package ${package.Controller};

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.beone.admin.exception.MvcException;
import com.beone.admin.annotation.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.beone.admin.common.AdminConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import java.io.Serializable;

#if(${restControllerStyle})
import org.springframework.web.bind.annotation.RestController;
#else
import org.springframework.stereotype.Controller;
#end
#if(${superControllerClassPackage})
import ${superControllerClassPackage};
#end

#if(${table.comment})
    #set($title=${table.comment})
#else
    #set($title=${entity})
#end

/**
 * @Title $!{table.comment} 前端控制器
 * @Author ${author}
 * @Version 1.0 on ${date}
 * @Copyright 贝旺科权
 */
#if(${restControllerStyle})
@RestController
#else
@Controller
#end
@RequestMapping("#if(${package.ModuleName})/${package.ModuleName}#end/#if(${controllerMappingHyphenStyle})${controllerMappingHyphen}#else${table.entityPath}#end")
@Api(value = "${title}管理模块", tags = {"${title}管理接口"})
#if(${kotlin})
class ${table.controllerName}#if(${superControllerClass}) : ${superControllerClass}()#end

#else
#if(${superControllerClass})
public class ${table.controllerName} extends ${superControllerClass} {
#else
public class ${table.controllerName} {
#end

private static final Logger log = LoggerFactory.getLogger(${table.controllerName}.class);

@Autowired
private ${table.serviceName} ${table.entityPath}Service;

    /**
     * $!{table.comment}管理 入口 --> index
     * @return
     */
    @GetMapping("")
    public String index() throws MvcException{
        return "${table.entityPath}/index";
    }

    /**
     * $!{table.comment}管理 显示添加页面
     * @return
     */
    @GetMapping("/showAdd")
    public String showAdd()  throws MvcException{
        return "${table.entityPath}/form";
    }


    /**
     * $!{table.comment}管理 显示修改页面
     * @return
     */
    @GetMapping("/showUpdate/{id}")
    public String showUpdate(@PathVariable("id") String id, ModelMap model) throws MvcException{
        try {
            model.addAttribute("id", id);
            model.addAttribute("entity", ${table.entityPath}Service.selectById(id));
        }catch (Exception e) {
            log.error("showUpdate 异常 e = {}", e);
            throw new MvcException(e.getMessage());
        }
        return "${table.entityPath}/form";
    }

    /**
     * $!{table.comment}管理分页列表 JSON
     * @param entity
     * @param currPage
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "${title}列表", notes = "${title}列表分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", required = true
                    , paramType = "param", dataType = "int"),
            @ApiImplicitParam(name = "rows", value = "每页最大显示记录数", required = true
                    , paramType = "param", dataType = "int")
    })
    @RequestMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE
            , method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Object showList(${table.entityName} entity,
    @RequestParam(value = "page", defaultValue = AdminConstants.DEFAULT_PAGE_NUM) int currPage,
    @RequestParam(value = "rows", defaultValue = AdminConstants.PAGE_SIZE) int pageSize) {
            return  ${table.entityPath}Service.get${table.entityName}Pagination(entity, currPage, pageSize);
    }

    /**
     *  保存$!{table.comment}
     * @param entity
     * @return
     */
    @ApiOperation(value = "保存${title}", notes = "保存${title} 主键属性为空新增,否则修改")
    @ApiImplicitParam(name = "entity", value = "{title}详细实体", required = true
            , paramType = "form", dataType = "object")
    @Log(desc = "${title}", type = Log.LOG_TYPE.SAVE)
    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object save(${table.entityName} entity) {
        if(${table.entityPath}Service.insertOrUpdate(entity)){
            return super.responseResult("success", "成功！");
        }
        return super.responseResult("fail", "失败！");
    }

    /**
     * 删除$!{table.comment}
     * @param id
     * @return
     */
    @ApiOperation(value = "删除${title}", notes = "根据${title}Id，删除${title}")
    @ApiImplicitParam(name = "id", value = "实体ID", required = true, paramType = "param", dataType = "object")
    @Log(desc = "删除${title}", type = Log.LOG_TYPE.DEL)
    @PostMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object delete(@PathVariable("id") Serializable id) {
        if(${table.entityPath}Service.deleteById(id)){
            return super.responseResult("success", "成功！");
        }
        return super.responseResult("fail", "失败！");
    }
}
#end