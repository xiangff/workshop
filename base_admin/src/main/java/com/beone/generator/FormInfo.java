package com.beone.generator;

import java.util.List;

/**
 * @title  表单信息
 * @Author 覃球球
 * @Version 1.0 on 2018/11/2.
 * @Copyright 贝旺科技
 */
public class FormInfo {

    private String tableName;
    private String comment;
    private String entityName;
    private List<FormField> fields;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public List<FormField> getFields() {
        return fields;
    }

    public void setFields(List<FormField> fields) {
        this.fields = fields;
    }
}
