package com.beone.admin.service;

import com.base.ISuperService;
import com.beone.admin.entity.BasePermission;
import com.beone.admin.entity.BaseRole;
import com.beone.admin.utils.PaginationGatagridTable;


import java.util.Collection;
import java.util.List;

/**
 * @Title 运维数据_角色信息 服务类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BaseRoleService extends ISuperService<BaseRole> {

    /**
     * 获取所有可用的角色信息
     * @return
     */
    List<BaseRole>  getAllUsedRoles();


    /**
     * 保存角色信息
     * @param role
     * @return
     */
    boolean saveRole(BaseRole role);

    /**
     * 删除角色信息
     * @param roleId
     * @return
     */
    boolean delRole(Integer roleId);

    /**
     * 分页显示后台角色列表
     * @param role
     * @param currPage  当前页码
     * @param pageSize  每页显示记录数
     * @return
     */
    PaginationGatagridTable getRolePagination(BaseRole role, int currPage, int pageSize);

    /**
     * 根据角色id查询对应权限
     * @param roleId 角色id
     * @return 格式化为easyui-tree适用的权限字符串
     */
    Collection<BasePermission> showRolePermissionses(Integer roleId);


    /**
     * 根据角色Id，选中属于该Id的所有权限
     * @param roleId
     * @return
     */
    Collection<BasePermission> setOwnRolePermissions(Integer roleId);
}
