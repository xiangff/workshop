package com.beone.admin.service;

import com.base.ISuperService;
import com.beone.admin.entity.BaseRoleUser;

import java.util.List;

/**
 * @Title 运维数据_用户对应的角色 服务类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BaseRoleUserService extends ISuperService<BaseRoleUser> {

    /**
     * 根据用户Id, 删除角色用户关系
     * @param userId
     */
     void deleteRoleUserRelationByUserId(Integer userId);
     /**
     * 根据用户Id, 删除角色用户关系
     * @param roleId
     */
     void deleteRoleUserRelationByRoleId(Integer roleId);


    /**
     * 根据用户Id 获取用户拥有的角色信息
     * @param userId
     * @return
     */
    List<BaseRoleUser>  getRoleUserByUserId(Integer userId);
}
