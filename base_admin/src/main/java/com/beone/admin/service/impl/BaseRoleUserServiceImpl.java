package com.beone.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.base.SuperServiceImpl;
import com.beone.admin.entity.BaseRoleUser;
import com.beone.admin.mapper.BaseRoleUserMapper;
import com.beone.admin.service.BaseRoleUserService;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Title 运维数据_用户对应的角色 服务实现类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@Service("BaseRoleUserService")
public class BaseRoleUserServiceImpl extends SuperServiceImpl<BaseRoleUserMapper, BaseRoleUser> implements BaseRoleUserService {
    /**
     * 根据用户Id, 删除角色用户关系
     * @param userId
     */
    public void deleteRoleUserRelationByUserId(Integer userId){
        EntityWrapper<BaseRoleUser> ew = new EntityWrapper<BaseRoleUser>();
        ew.eq("user_id", userId);
        baseMapper.delete(ew);
    }
    /**
     * 根据角色Id, 删除角色用户关系
     * @param roleId
     */
    public void deleteRoleUserRelationByRoleId(Integer roleId){
        EntityWrapper<BaseRoleUser> ew = new EntityWrapper<BaseRoleUser>();
        ew.eq("role_id", roleId);
        baseMapper.delete(ew);
    }

    /**
     * 根据用户Id 获取用户拥有的角色信息
     * @param userId
     * @return
     */
    public List<BaseRoleUser> getRoleUserByUserId(Integer userId){
        EntityWrapper<BaseRoleUser> ew = new EntityWrapper<BaseRoleUser>();
        ew.eq("user_id", userId);
        return baseMapper.selectList(ew);
    }
}
