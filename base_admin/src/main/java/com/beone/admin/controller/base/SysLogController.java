package com.beone.admin.controller.base;


import com.beone.admin.common.AdminConstants;
import com.beone.admin.service.SysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import com.base.SuperController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Title 日志管理 前端控制器
 * @Author 覃球球
 * @Version 1.0 on 2018-10-22
 * @Copyright 贝旺科权
 */
@Api(value = "系统操作日志管理模块", tags = {"系统操作日志管理接口"})
@Controller
@RequestMapping("/system/log")
public class SysLogController extends SuperController {
    @Autowired
    private SysLogService logService;

    /**
     * 日志管理 入口
     *
     * @return
     */
    @RequestMapping("")
    public String index() {
        return "base/log/index";
    }

    /**
     * 日志分页列表 JSON
     *
     * @param currPage
     * @param pageSize
     * @return
     */
    @PostMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiOperation(value = "操作日志列表", notes = "操作日志后台列表分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", required = true
                    , paramType = "param", dataType = "int"),
            @ApiImplicitParam(name = "rows", value = "每页最大显示记录数", required = true
                    , paramType = "param", dataType = "int"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", required = false
                    , paramType = "param", dataType = "string"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", required = false
                    , paramType = "param", dataType = "string")
    })
    public Object getUserList(HttpServletRequest request,
                              @RequestParam(value = "page", defaultValue = AdminConstants.DEFAULT_PAGE_NUM) int currPage,
                              @RequestParam(value = "rows", defaultValue = AdminConstants.PAGE_SIZE) int pageSize) {
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");
        return logService.getSysLogPagination(startTime, endTime, currPage, pageSize);
    }
}

