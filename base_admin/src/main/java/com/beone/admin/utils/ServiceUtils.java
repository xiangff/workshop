package com.beone.admin.utils;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import com.beone.admin.utils.result.Params;
import com.base.common.util.algorithm.AESUtil;
import com.base.common.util.algorithm.Base64;
import com.base.common.util.algorithm.RSAUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @title Service处理工具类
 * @Author 覃球球
 * @Version 1.0 on 2018/1/26.
 * @Copyright 长笛龙吟
 */
public class ServiceUtils {

    //日志
    private static final Logger log = LoggerFactory.getLogger(ServiceUtils.class);

    /**
     * UI DatagridTable 分页 JSON数据封装
     */
    public static PaginationGatagridTable createGatagridTableJson(Pagination page, Collection json) {
        PaginationGatagridTable table = new PaginationGatagridTable();
        /*table.setCurrPage(page.getCurrent());
        table.setTotal(page.getTotal());
        table.setTotalPage(page.getPages());
        table.setRows(json);*/
        if(page != null){
            table.setCount(page.getTotal());
        }else {
            table.setCount(json.size());
        }
        table.setData(json);
        return table;
    }

    /**
     * 返回值封装
     *
     * @param flag
     * @param msg
     * @param result
     * @param token
     * @return
     */
    public static Map<String, Object> getResponseResult(int flag, String msg, Object result, String token) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("flag", flag);
        map.put("msg", msg);
        if (result != null) {
            map.put("result", result);
        }
        if (StringUtils.isNotBlank(token)) {
            map.put("token", token);
        }
        log.info("返回:{}", JSON.toJSONString(map));
        return map;
    }

    /**
     * 返回值封装
     *
     * @param flag
     * @param msg
     * @return
     */
    public static Map<String, Object> getResponseResult(int flag, String msg) {
        return getResponseResult(flag, msg, null, null);
    }

    /**
     * rea aes解密
     *
     * @param params
     * @param privateKey
     * @param clazz
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> T decryptToObj(Params params, String privateKey, Class<T> clazz) throws Exception {
        byte[] bytes = Base64.decode(params.getKey());
        String aseKey = new String(RSAUtil.decryptByPrivateKey(bytes, privateKey));
        log.info("aseKey={}", aseKey);
        String data = new String(AESUtil.decrypt(aseKey, params.getData()));
        log.info("data={}", data);
        return JSON.parseObject(data, clazz);
    }

    /**
     * rea aes解密
     *
     * @param params
     * @param privateKey
     * @param clazz
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> List<T> decryptToList(Params params, String privateKey, Class<T> clazz) throws Exception {
        byte[] bytes = Base64.decode(params.getKey());
        String aseKey = new String(RSAUtil.decryptByPrivateKey(bytes, privateKey));
        log.info("aseKey={}", aseKey);
        String data = new String(AESUtil.decrypt(aseKey, params.getData()));
        log.info("data={}", data);
        return JSON.parseArray(data, clazz);
    }
}
