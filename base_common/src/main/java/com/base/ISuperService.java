package com.base;

import com.baomidou.mybatisplus.service.IService;

/**
 * @title  业务层接口抽象
 * @Author 覃球球
 * @Version 1.0 on 2017/12/18.
 * @Copyright 长笛龙吟
 */
public interface ISuperService<T> extends IService<T> {

}
